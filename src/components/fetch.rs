use serde::{Serialize, Deserialize};
use reqwest::Error;

#[derive(Default, Debug, Clone, PartialEq, Serialize, Deserialize)]
#[serde(rename_all = "camelCase")]
pub struct CarbonRoot {
    pub data: Vec<CarbonRegion>,
}

#[derive(Default, Debug, Clone, PartialEq, Serialize, Deserialize)]
#[serde(rename_all = "camelCase")]
pub struct CarbonRegion {
    pub regionid: i64,
    pub dnoregion: String,
    pub shortname: String,
    pub data: Vec<CarbonData>,
}

#[derive(Default, Debug, Clone, PartialEq, Serialize, Deserialize)]
#[serde(rename_all = "camelCase")]
pub struct CarbonData {
    pub from: String,
    pub to: String,
    pub intensity: Intensity,
    pub generationmix: Vec<Generationmix>,
}

#[derive(Default, Debug, Clone, PartialEq, Serialize, Deserialize)]
#[serde(rename_all = "camelCase")]
pub struct Intensity {
    pub forecast: i64,
    pub index: String,
}

#[derive(Default, Debug, Clone, PartialEq, Serialize, Deserialize)]
#[serde(rename_all = "camelCase")]
pub struct Generationmix {
    pub fuel: String,
    pub perc: f64,
}

pub async fn fetch(name: String) -> Result<CarbonRoot, Error> {
    let url = format!("{}{}","https://api.carbonintensity.org.uk/regional/", name);
    reqwest::get(url)
        .await?
        .json::<CarbonRoot>()
        .await
}